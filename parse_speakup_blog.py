# -*- coding: utf-8 -*-
#
# Author:   Dario Necco
# Company:  Dartie
#
# This script allows to ...
import locale
import os
import sys
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import time
import shutil
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
from lxml import html
import requests
from bs4 import BeautifulSoup
from urllib import request
# import chardet  # uncomment only if necessary, it requires installation
import pickle

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style
    init()
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author
__version__ = '1.0'
intern_version = '0001'

# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
    dirapp_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))
    dirapp_bundle = dirapp
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

print('Working dir: \"' + dirapp + '\"')
welcome_text = '{char1} {appname} v.{version} {char2}'.format(char1='-' * 5,
                                                              appname=os.path.splitext(os.path.basename(__file__))[0],
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)
                        # help='Increase output verbosity. Output files don\'t overwrite original ones'

    parser.add_argument("-c", "--cache", dest="cache", action='store_true', help="use cache values rather than parse the website")

    parser.add_argument("-e", "--end", dest="end", default=' ', help="last page to get")

    # parser.add_argument("-op", "--output", dest="output_path", help="qac output path. it has given in input to this application")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    # parser.add_argument("source_file", help="source file must be analyzed")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args

# global variable which need to be populated
link_title__dict = dict()
title_link__dict = dict()
links_list = []


def get_all_links(url, args=None):
    print(url)
    links_list.append(url)
    speakupFile = request.urlopen(url)
    speakupHtml = speakupFile.read()
    speakupFile.close()
    soup = BeautifulSoup(speakupHtml, features="lxml")

    articles_nodes = soup.find_all('a',attrs={"class": "btn"})

    for an in articles_nodes:
        title = an.attrs['title']
        link = an.attrs['href']
        link_title__dict[link] = title

        if title not in title_link__dict:
            title_link__dict[title] = []
        title_link__dict[title].append(link)

    # get older post page link
    try:
        more_entries_link = soup.find('div',attrs={"class": "more_entries"}).find(attrs={'class': 'fr'}).find('a').attrs['href']
    except:
        more_entries_link = ''  # reached the last page

    if not more_entries_link == '' and not url.endswith(args.end):
        get_all_links(more_entries_link, args)


def get_article_content(filter=None):
    link_content__dict = dict()
    link_title_content__dict = dict()

    for title in title_link__dict:
        if filter:
            if not re.match(filter, title):
                continue  # discard content if it doesn't match what I need
            else:
                pass

        link_list = title_link__dict[title]

        if not isinstance(link_list, list):
            link_list = [link_list]

        for link in link_list:
            speakupFile = request.urlopen(link)
            speakupHtml = speakupFile.read()
            speakupFile.close()
            soup = BeautifulSoup(speakupHtml, features="lxml")

            content_node = soup.find('div', attrs={'class': 'entry'})
            content_nodes = content_node.contents

            text_content = []
            for cn in content_nodes:
                if isinstance(cn, str):
                    text_content.append(cn)
                else:
                    text_content.append(cn.text)

            sep_char = ''
            if ':' in title:
                sep_char = ':'
            elif '-' in title:
                sep_char = '-'
            elif '–' in title:
                sep_char = '–'

            if not sep_char == '':
                # works for "idioms: ...."
                # works for "vocabulary - ...."
                # works for "vocabulary – ...."
                # works for "song - ...."
                title, title2 = title.split(sep_char)
            else:
                # works for "how do you say..."
                title2 = [x for x in text_content if not x.strip() == ''][0]
                if '\n' in title2:
                    title2 = title2.split('\n')[0]

                sep_char = ' '

            print('{}{}{} -> {}'.format(title, sep_char, title2, link))


            link_content__dict[link] = text_content
            link_title_content__dict[link] = title, title2, text_content

    return link_content__dict, link_title_content__dict


def save_cache(elements_dict=()):
    """

    :param elements_dict: { 'variable_name' : content }
    :return: -
    """
    cache_py_content = ''
    for varname, varvalue in elements_dict.items():
        if isinstance(varvalue, dict) or isinstance(varvalue, list):
            cache_py_content += varname + '=' + pprint.pformat(varvalue)
            cache_py_content += '\n\n'

            # write dictionaries and list to plain text
    with io.open('cache.py', 'w', encoding='utf-8', errors='ignore') as write_cache:
        write_cache.write(cache_py_content)


def main(args=None):
    if args is None:
        args = check_args()

    global link_title__dict
    global title_link__dict
    global links_list

    url = 'http://blog.speakuponline.it/'

    if not args.cache:
        # parse all home pages in order to get article links and populate link_title__dict and title_link__dict
        get_all_links(url, args)

        # get the content from each article
        # link_content__dict, link_title_content__dict = get_article_content(re.compile(r'^how do you say', re.IGNORECASE))
        # link_content__dict, link_title_content__dict = get_article_content(re.compile(r'^idioms', re.IGNORECASE))
        # link_content__dict, link_title_content__dict = get_article_content(re.compile(r'^VOCABULARY', re.IGNORECASE))
        link_content__dict, link_title_content__dict = get_article_content(filter=None)

        save_cache(
            {
                'link_content__dict': link_content__dict,
                'link_title_content__dict': link_title_content__dict,
                'links_list': links_list
            }
        )
    else:  # load from cache
        try:
            from cache import link_content__dict, link_title_content__dict, links_list
        except:
            link_content__dict, link_title_content__dict = {}, {}


    print('Done!')


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

# DONE: save cache
# DONE: choose whether load from cache or online
# DONE: title can be the same, the value must be a list
# TODO : dump to json file